package exam7.utils;

import exam7.models.Client;
import exam7.models.Dish;
import exam7.models.Order;
import exam7.models.Restaurant;
import exam7.repositories.ClientRepository;
import exam7.repositories.DishRepository;
import exam7.repositories.OrderRepository;
import exam7.repositories.RestaurantRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Configuration
public class DatabasePreloader {
    private final Random r = new Random();

    @Bean
    CommandLineRunner initDatabase(RestaurantRepository rr, DishRepository dr, OrderRepository or, ClientRepository cr){
        return(args) -> {
            rr.deleteAll();
            dr.deleteAll();
            or.deleteAll();

            List<Restaurant> restaurants = new ArrayList<>();
            List<Dish> dishes = new ArrayList<>();
            List<Order> orders = new ArrayList<>();
            List<Client> clients = new ArrayList<>();

            int restaurantAmount = r.nextInt(7) + 2;
            int dishAmount = r.nextInt(22) + 7;
            int orderAmount = r.nextInt(17) +7;
            int clientAmount = r.nextInt(17) + 7;

            for(int i = 0; i < restaurantAmount; i++){
                GenerateData.PlaceName randomPlace = GenerateData.randomPlace();
                Restaurant rest = new Restaurant(randomPlace.name.trim(), randomPlace.description.trim());
                restaurants.add(rest);
            }
            rr.saveAll(restaurants);

            for(int i = 0; i < dishAmount; i++){
                GenerateData.DishName randomDish = GenerateData.randomDish();
                Restaurant randomRest = restaurants.get(r.nextInt(restaurants.size()));
                int randomPrice = r.nextInt(100) + 30;

                Dish d = new Dish(randomRest, randomDish.name.trim(), randomDish.type.trim(), randomPrice);
                dishes.add(d);
            }
            dr.saveAll(dishes);

            for(int i = 0; i < clientAmount; i++){
                Client client = new Client(GenerateData.randomPersonName(), GenerateData.randomEmail());
                clients.add(client);
            }
            cr.saveAll(clients);

            for(int i = 0; i < orderAmount; i++){
                Dish randomDish = dishes.get(r.nextInt(dishes.size()));
                Client randomClient = clients.get(r.nextInt(clients.size()));

                Order o = new Order(randomClient, randomDish);
                orders.add(o);
            }
            or.saveAll(orders);
        };
    }
}