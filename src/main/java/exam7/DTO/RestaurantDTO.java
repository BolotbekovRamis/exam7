package exam7.DTO;

import exam7.models.Restaurant;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)

public class RestaurantDTO {
    private String name;
    private String description;

    public static RestaurantDTO from(Restaurant rest){
        return builder()
                .name(rest.getName())
                .description(rest.getDescription())
                .build();
    }
}