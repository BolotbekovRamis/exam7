package exam7.DTO;

import exam7.models.Dish;
import lombok.*;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)

public class DishDTO {
    private String name;
    private int price;

    public static DishDTO from(Dish dish){
        return builder()
                .name(dish.getName())
                .price(dish.getPrice())
                .build();
    }
}