package exam7.DTO;

import exam7.models.Order;
import lombok.*;
import java.time.LocalDateTime;

@Data
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)

public class OrderDTO {
    private String dish_name;
    private String restaurant_name;
    private LocalDateTime order_time;

    public static OrderDTO from(Order order){
        return builder()
                .dish_name(order.getDish().getName())
                .restaurant_name(order.getDish().getRestaurant().getName())
                .build();
    }
}