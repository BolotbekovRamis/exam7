package exam7.controllers;


import exam7.DTO.DishDTO;
import exam7.restaurant.service.DishService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiPageable
@RestController
@RequestMapping("/dishes")
public class DishController {
    private final DishService ds;

    public DishController(DishService ds) {
        this.ds = ds;
    }

    @GetMapping("/restaurant/{restaurantId}")
    public Slice<DishDto> findAll(@PathVariable String restaurantId){
        return ds.findAllDishesByRestaurant(restaurantId);
    }
}