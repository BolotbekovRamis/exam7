package exam7.controllers;


import exam7.DTO.RestaurantDTO;
import exam7.restaurant.service.RestaurantService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiPageable
@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    private final RestaurantService rs;

    public RestaurantController(RestaurantService rs) {
        this.rs = rs;
    }

    @GetMapping()
    public Slice<RestaurantDTO> findAll(@ApiIgnore Pageable pageable){
        return rs.findAllRestaurants(pageable);
    }
}
