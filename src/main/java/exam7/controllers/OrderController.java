package exam7.controllers;

import exam7.DTO.OrderDTO;
import exam7.services.ClientService;
import exam7.restaurant.service.OrderService;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@ApiPageable
@RestController
@RequestMapping("/orders")
public class OrderController {
    private final OrderService os;
    private final ClientService cs;

    public OrderController(OrderService os, ClientService cs) {
        this.os = os;
        this.cs = cs;
    }

    @GetMapping("/orders")
    public List<OrderDto> findOrdersByClient(Authentication authentication){
        return os.findOrdersByClient(authentication);
    }

}
