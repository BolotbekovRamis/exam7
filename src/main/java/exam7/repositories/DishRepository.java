package exam7.repositories;

import exam7.models.Dish;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DishRepository extends PagingAndSortingRepository<Dish, String> {
}