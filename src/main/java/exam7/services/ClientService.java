package exam7.services;

import exam7.models.Client;
import exam7.repositories.ClientRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    private final ClientRepository cr;

    public ClientService(ClientRepository cr) {
        this.cr = cr;
    }

    public Client getClientByEmail(String email){
        return cr.getClientByEmail(email);
    }

    public Client getClientById(String id){
        return cr.getById(id);
    }
}
