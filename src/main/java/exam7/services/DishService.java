package exam7.services;

import exam7.DTO.DishDTO;
import exam7.repositories.DishRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;
import springfox.documentation.annotations.ApiIgnore;


@Service
public class DishService {

    private final DishRepository dr;

    public DishService(DishRepository dr) {
        this.dr = dr;
    }

    public Slice<DishDTO> findAllDishesByRestaurant(@ApiIgnore Pageable pageable, String restaurantId){
        return dr.findAllByRestaurantId(pageable, restaurantId).map(DishDTO::from);
    }
}