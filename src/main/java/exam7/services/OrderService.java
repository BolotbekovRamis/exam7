package exam7.services;

import exam7.DTO.OrderDTO;
import exam7.models.Client;
import exam7.models.Dish;
import exam7.models.Order;
import exam7.repositories.ClientRepository;
import exam7.repositories.DishRepository;
import exam7.repositories.OrderRepository;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final OrderRepository or;
    private final ClientRepository cr;
    private final DishRepository dr;

    public OrderService(OrderRepository or, ClientRepository cr, DishRepository dr) {
        this.or = or;
        this.cr = cr;
        this.dr = dr;
    }

    public List<OrderDTO> findOrdersByClient
}
