package exam7.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.UUID;

@Data
@Document(collection = "dishes")
public class Dish {

    @Id
    private String id = UUID.randomUUID().toString();

    @DBRef
    private Restaurant restaurant;
    private String name;
    private int price;
    private String type;

    public Dish(Restaurant restaurant, String name, String type, int price) {
        this.restaurant = restaurant;
        this.name = name;
        this.price = price;
        this.type = type;
    }
}
