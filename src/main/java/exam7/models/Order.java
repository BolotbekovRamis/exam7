package exam7.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@Document(collection = "orders")
public class Order {

    @Id
    private String id = UUID.randomUUID().toString();

    @DBRef
    private Client client;

    @DBRef
    private Dish dish;

    private LocalDateTime order_time;

    public Order(Client client, Dish dish) {
        this.client = client;
        this.dish = dish;
        order_time = LocalDateTime.now();
    }
}
